# -*- coding: utf-8 -*-
"""3-module2.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/11OA3ag67oV5rF_KCIE_uLSF7HepFg5vx
"""

#approach 1
result=[]
nums=[1,2,3,4,5]
def square_fn(x):
  return x*x
for i in nums:
  result.append(square_fn(i))
print(result)

#approach 2
#for one iterable
nums=[1,2,3,4,5]
def square_fn(x):
  return x*x
result=map(square_fn,nums) 
print(result)
print(type(result))
print(list(result))

#for one or more iterables
#map with multiple iterables
def prod_sum(n1,n2,n3):
  return(n1*n2)+n3
num1,num2,num3=[1,5,10],[2,6,11],[3,7,12]               #here it takes 1 2 3 as first iterable and next 5 6 7 as next iterable and so on
print(tuple(map(prod_sum,num1,num2,num3)))               #can be list also in place of tuple
words=['sat','bat','cat','mat'] 
#map() can listify the list of strings individully
word_map=map(list,words)            #to convert from string to list
print(list(word_map))

#2nd order map function
def listify(a,b) -> list:
  return [a,b]

from types import FunctionType
def list_concat(x,y) ->FunctionType:
  prod=x*y
  sum=x+y
  return listify(prod,sum)

second_map=map(list_concat,listify(1,2),listify(2,2))
print(list(second_map))

#approach1
result=[]
nums=[1,2,7,3,3,4,5]
def less_than_four(x):
  return x<4
for n in nums:
  if less_than_four(n):
    result.append(n)
print(result)             #print(tuple(result))     gives result in tuple form i.e.(1,2,3)



#filter function - returns true or false
nums=[1,4,5,3,2]
def less_than_four(x):
  return x<4
result=filter(less_than_four,nums) 
print(result)
print(list(result))
print(tuple(result))          #whatever we give first it considers it so it gives empty list

#example 2
nums=[1,3,9,12,15,18]
def is_even(n):
  return n%2==0   #filtering condition
list(filter(is_even,nums))

#reduce()
#approach1
nums=[1,2,3,4,5]
result=1
def product(x,y):
  return x*y
for n in nums:
  result=(product(result,n))
  print(result)

#approch 2    #gives one single value as output
from functools import reduce
nums=[1,2,3,4,5]
def product(x,y):
  return x*y
result=reduce(product,nums)     
print(result)

#reduce function with 3 arguments
#3rd paramter of reduce() is intializer
#def value of intializer is empty
def sum(c,d):
  return c+d
r1=reduce(sum,["dhruv-","ganesh"],"srikanth-")
print(type(r1))
print(r1)

#zip() function
names=["john","stefen","taylor"]
roll_no=[1,4,3,6]
marks=[80,78,45,23,54]
mapped=zip(names,roll_no,marks)
print(list(mapped))

"""**LAMBDA FUNCTION**"""

var=lambda x,y:x*y
var(4,6)

str1='POojitha'
#lambda returns a function onbject
rev_upper=lambda string: string.upper()[::-1]
print(rev_upper(str1))

"""**LAMBDA WITH MAP,FILTER,REDUCE,ZIP**

lambda functions are especially useful when used in conjunctions with functions like map,filter,reduce,zip
"""

nums1=[1,2,3,4,4,5]
result1=map(lambda x: x*x,nums1)
print(list(result1))

nums2=[1,92,3,4,5]
result2=filter(lambda x:x<4,nums2)
print(list(result2))

from functools import reduce
nums3=[1,3,4,6,9]
reduce_test=reduce(lambda g,h: g-h,nums3)

num1=[1,2,3,4,5]
num2=[6,7,8,9,10]
res=list(map(lambda x: sum(x),zip(num1,num2)))
print(res)                               #after zipping we will get tuple

squares=[]
for x in range(6):
  squares.append(x**2)
print(squares)

#approach 2
squares=[x**2 for x in range(6)]
print(squares)

lt4=[x*2 for x in range(6) if x<4]
print(lt4)

#creating list of squares of natural numbers 
rng1=range(1,6)
Newlist1=[i*i for i in rng1]
print(Newlist1)

#creating list of odd numbers
tup1=(1,2,3,4,5,6,7,8)
NewList2=[i for i in tup1 if i%2!=0]
print(NewList2)

#creating list of charcters (in uppercase) of a string
str='Hello'
NewList3=[i.upper() for i in str]
print(NewList3)

#create  a list that only include the members of the given fellowship
#list that have 6 characters or more
fellowship=['Bettie','Frank','Mystica','Ballina','Hardy','Mendonza']
new_fellowship=[member for member in fellowship if len(member)>=6]
print(new_fellowship)

#grade
list=[60,47,53,78,87,32]
a=[str(i)+" A"  if i>=60 else str(i)+" B" if i>=50 else str(i)+" C" if i>=40 else "Fail" for i in list]
print(a)                                                                        #str(i) is used becaues 60 is a integer  and str is string so that they dont concatenate

#print table
n=int(input("enter a number:"))
[(i,"*",j,"==",(i*j)) for i in range(2,n+1) for j in range(1,11)]

#largest of three numbers
var3=lambda x,y,z : [x if x > y and x >z else y if y > z else z]
var3(78,60,47)

"""assignment
1.use map to create a new list by chnaging each country to uppercase in the countries list

2.use filter to filter out countries having exactly six charcaters

3.create a function returning a dictionary,where keys stand for starting letters of countries and values are the number of country name sstarting with that letter

4.
for all the numbers 1-1000 us e a nested list/dictionary comprehension to find how many numbers are divisible by each single digit 3-9 and print the isngle digit which has the highest number of multiples
 
"""

#1
list1=["India","Belgium","France","Australia"]
def upper_case(x):
  y=x.upper()
  return y
result=map(upper_case,list1)  
print(list(result))

#2
def max_six(x):
  return len(x)==6
result=filter(max_six,list1)
print(list(result))

#3
starting_letters=["a","i","c","b"]
no_of_countries=[2,4,5,6,7]
list1=zip(starting_letters,no_of_countries)
print(dict(list1))

multiples=[[num for num in range(1,1001) if num%i ==0] for i in range(2,10)]
multiples_count={str(i): len(multiples[i-2])for i in range(2,10)}
max_digit=max(multiples_count,key=multiples_count.get)

print("n0 of multiples from 1 to 9:")
print(multiples_count)
print(f"the single digit with highest order multiples is {max_digit} with {multiples_count[max_digit]}")